job "ups-monitoring" {
  name = "UPS Monitoring (NUT)"
  type = "service"
  region = "global"
  datacenters = ["proxima"]
  namespace = "default"

  group "server-rack-2" {
    count = 1

    network {
      mode = "bridge"
      port "nut" { to = 3493 }
    }

    constraint {
      attribute = "${attr.unique.hostname}"
      value = "server-rack-2"
    }

    service {
      name = "ups-2-nut"
      port = "3493"

      connect {
        sidecar_service {}
      }
    }
    
    task "nut" {
      driver = "docker"

      config {
        image = "instantlinux/nut-upsd:latest"
        privileged = true

        volumes = [
          "secrets/nut-upsd-password:/run/secrets/nut-upsd-password",
          "local/upsmon.conf:/etc/nut/local/upsmon.conf"
        ]

        ports = ["nut"]
      }

      vault {
        policies = ["ups-monitor"]
      }

      template {
        data = <<EOH
          SECRET=nut-upsd-password

          {{ with secret "ups-monitor/ups-2" }}
          DRIVER={{ index .Data.data "DRIVER" }}
          NAME={{ index .Data.data "NAME" }}
          SERIAL={{ index .Data.data "SERIAL" }}
          {{ end }}
        EOH

        destination = "secrets/nut.env"
        env = true
      }

      template {
        data = <<EOH
{{ with secret "ups-monitor/ups-2" }}
{{ index .Data.data "PASSWORD" }}
{{ end }}
        EOH

        destination = "secrets/nut-upsd-password"
      }


      template {
        data = <<EOH
[[ fileContents "./config/upsmon.template.conf" ]]
        EOH

        destination = "local/upsmon.conf"
      }

      resources {
        device "051d/usb/0003" {}
      }
    }
  }

  group "server-rack-3" {
    count = 1

    network {
      mode = "bridge"

      port "nut" { to = 3493 }
    }

    constraint {
      attribute = "${attr.unique.hostname}"
      value = "server-rack-3"
    }

    service {
      name = "ups-1-nut"
      port = "3493"

      connect {
        sidecar_service {}
      }
    }
    
    task "nut" {
      driver = "docker"

      config {
        image = "instantlinux/nut-upsd:latest"
        privileged = true

        volumes = [
          "secrets/nut-upsd-password:/run/secrets/nut-upsd-password",
          "local/upsmon.conf:/etc/nut/local/upsmon.conf"
        ]

        ports = ["nut"]
      }

      vault {
        policies = ["ups-monitor"]
      }

      template {
        data = <<EOH
          SECRET=nut-upsd-password

          {{ with secret "ups-monitor/ups-1" }}
          DRIVER={{ index .Data.data "DRIVER" }}
          NAME={{ index .Data.data "NAME" }}
          SERIAL={{ index .Data.data "SERIAL" }}
          {{ end }}
        EOH

        destination = "secrets/nut.env"
        env = true
      }

      template {
        data = <<EOH
{{ with secret "ups-monitor/ups-1" }}
{{ index .Data.data "PASSWORD" }}
{{ end }}
        EOH

        destination = "secrets/nut-upsd-password"
      }


      template {
        data = <<EOH
[[ fileContents "./config/upsmon.template.conf" ]]
        EOH

        destination = "local/upsmon.conf"
      }

      resources {
        device "051d/usb/0002" {}
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
