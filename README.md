# UPS Monitor

[[_TOC_]]

## Description

[NUT](https://networkupstools.org/) monitoring services for the various UPS devices within within hQ.

## Devices

There are 3 models of UPS currently in use within hQ:

- APC Smart UPS 1500VA (DLA1500RM120)
- APC Smart UPS 1000VA (SMT1000I)
- APC Smart UPS 3000VA (SRT3000XLI)

Each of these are connected to a Raspberry Pi via USB which is passed into the jobs task allowing NUT to communicate
with the UPS and pull metrics from them.
